import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const config = {
  apiKey: 'AIzaSyBkJIdAvUNZiQZJxd6v0m6PLyLYsO7dhMY',
  authDomain: 'profiles-ad6c6.firebaseapp.com',
  databaseURL: 'https://profiles-ad6c6.firebaseio.com',
  projectId: 'profiles-ad6c6',
  storageBucket: 'profiles-ad6c6.appspot.com',
  messagingSenderId: '1007871411461'
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

export default firebase
