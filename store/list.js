import firebase from '../utils/firebase'

const db = firebase.firestore()

export const state = () => ({
  list: null,
  tags: []
})

export const mutations = {
  setList: (state, value) => {
    state.list = value
  },
  setTags: (state, value) => {
    state.tags = value
  }
}

export const actions = {
  async loadProfiles({ commit }, user) {
    const profileRef = db.collection('profiles')
    const profileList = {}
    await profileRef.get().then((listResult) => {
      listResult.forEach(function (doc) {
        profileList[doc.id] = doc.data()
      })
    })
    commit('setList', profileList)
  },
  async loadTags({ commit }, user) {
    const tagsRef = db.collection('tags')
    const tag = []
    await tagsRef.get().then((tagList) => {
      tagList.forEach(function (doc) {
        tag.push(doc.data().name)
      })
    })
    commit('setTags', tag)
  },
  addTag(context, tag) {
    const tagRef = db.collection('tags')

    tagRef.add({
      name: tag
    }).then(() => {
      context.dispatch('loadTags')
    })
  }

}
