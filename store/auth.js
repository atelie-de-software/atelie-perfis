import VuexPersistence from 'vuex-persist'
import firebase from '../utils/firebase'
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

const db = firebase.firestore()

export const state = () => ({
  user: null,
  profile: null,
  editing: false,
  tags: []
})

export const getters = {
  uid(state) {
    if (state.user && state.user.uid) return state.user.uid
    else return null
  },

  user(state) {
    return state.user
  },

  isAuthenticated(state) {
    return !!state.user && !!state.user.uid
  }
}

export const mutations = {
  setProfile: (state, value) => {
    state.profile = value
  },
  setUser: (state, value) => {
    state.user = value
  },
  updateDisplayName: (state, value) => {
    state.profile.displayName = value
  },
  updateDescription: (state, value) => {
    state.profile.description = value
  },
  updateEditing: (state, value) => {
    state.editing = value
  },
  pushTag: (state, value) => {
    state.editing = value
  },
  setTags: (state, value) => {
    state.tags = value
  }
}

export const actions = {
  loginGoogle({ commit, dispatch }) {
    const provider = new firebase.auth.GoogleAuthProvider()

    firebase
      .auth()
      .setPersistence(firebase.auth.Auth.Persistence.SESSION).then(function () {
        return firebase
          .auth()
          .signInWithPopup(provider)
          .then(function (result) {
            dispatch('createProfile', result.user)
          })
          .catch(function (error) {
            return error
          })
      })
  },
  createProfile({ commit }, user) {
    const profileRef = db.collection('profiles').doc(user.uid)

    profileRef.get().then((docSnapshot) => {
      if (docSnapshot.exists) {
        profileRef.onSnapshot((doc) => {
          commit('setProfile', doc.data())
          commit('setTags', docSnapshot.data().tags)
        })
      } else {
        profileRef.set({
          displayName: user.displayName,
          email: user.email,
          avatar: user.photoURL,
          description: user.description || ''
        })
      }
    })
  },
  authCheck(context) {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        context.commit('setUser', {
          uid: user.uid,
          email: user.email,
          displayName: user.displayName
        })
        context.commit('setProfile', {
          avatar: user.photoURL,
          email: user.email,
          displayName: user.displayName
        })
        context.dispatch('createProfile', user)
      }
    })
  },
  updateProfile(context) {
    const uid = context.getters.uid
    const profileRef = db.collection('profiles').doc(uid)
    const { displayName, description } = context.state.profile
    profileRef.update({
      displayName,
      description
    }).then(() => {
      context.commit('updateEditing', false)
    })
  },
  logout({ commit }) {
    firebase.auth().signOut()
    commit('setProfile', null)
    commit('setUser', null)
  },
  getUserTags(context) {
    const id = context.getters.uid
    const tagRef = db.collection('profiles').doc(id)
    tagRef.get().then((docSnapshot) => {
      if (docSnapshot.exists) {
        context.commit('setTags', docSnapshot.data().tags)
      }
    })
  },
  setTag(context, tag) {
    const uid = context.getters.uid
    const tagRef = db.collection('profiles').doc(uid)

    tagRef.update({
      tags: firebase.firestore.FieldValue.arrayUnion(tag[0])
    }).then(() => {
      context.dispatch('getUserTags')
    })
  },
  removeTag(context, tag) {
    const uid = context.getters.uid
    const tagRef = db.collection('profiles').doc(uid)

    tagRef.update({
      tags: firebase.firestore.FieldValue.arrayRemove(tag[0])
    }).then(() => {
      context.dispatch('getUserTags')
    })
  }
}

export const plugins = [vuexLocal.plugin]
