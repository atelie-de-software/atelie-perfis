import VuexPersistence from 'vuex-persist'
import firebase from '../utils/firebase'
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

const db = firebase.firestore()

export const state = () => ({
  profile: {
    avatar: '',
    displayName: '',
    description: ''
  },
  tags: {},
  darkMode: false
})

export const mutations = {
  setProfile: (state, value) => {
    state.profile = value
  },
  resetProfile: (state) => {
    state.profile = {
      avatar: '',
      displayName: '',
      description: ''
    }
  },
  setDarkMode: (state) => {
    state.darkMode = !state.darkMode
  }
}

export const actions = {
  load({ commit }, id) {
    const profileRef = db.collection('profiles').doc(id)
    profileRef.get().then((profile) => {
      commit('setProfile', profile.data())
    })
  }
}

export const plugins = [vuexLocal.plugin]
